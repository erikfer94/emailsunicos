var test = [
    'test.email+alex@kavak.com',
    'test.e.mail+bob.cathy@kavak.com',
    'testemail+david@ka.vak.com'
];
function explode(arr){
    var resp = '';
    for (let el of arr){
        resp = resp + el;
    }
    return resp
}
function reduceMails(mails){
    return mails.reduce((acc, val) =>{
        //segun las instrucciones el mail puede contener mayusculas y minusculas, por eso se hace la comparacion en case sensitive.
        //par hacer case sensitive descomentar la sigueinte linea
        //val = val.toLowerCase();
        if (acc.indexOf(val) < 0){
            acc.push(val)
        }
        return acc;
    }, [])
}
function cleanMails(emails){
    var cleanedEmails = [];
    for (let mail of emails) {
        mailParts = mail.split('@');
        if(mailParts.length >= 2) { //Si solo tnemos un elemento en el mail, es un mail invalido.
            let name = mailParts[0];
            mailParts.splice(0, 1);
            //Considerando el caso de que el dominio tenga mas de un arroba se busca unir todos los elementos restantes.
            let domain = explode(mailParts);
            name = name.substring(0, name.indexOf('+'));
            name = name.replace(/\./gi, '');
            //Si se quiere limpiar de caracteres raros al dominio descomentarla siguiente linea.
            //domain = domain.replace(/[^a-z0-9A-Z\.]/gi, '');
            cleanedEmails.push(name + '@' + domain);
        }
    }
    return cleanedEmails;
}
var emailsUnicos = function(emails) {
    let cleanedEmails = cleanMails(emails); //Se limpian los emails para regresar un nuevo array con los emails para enviar
    let reducedEmails = reduceMails(cleanedEmails); // Se reducen los emails a una lista donde ya no hay repetidos
    return reducedEmails.length; //Retornamos el tamaño del array
};
console.log(emailsUnicos(test));